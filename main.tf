terraform {
  backend "http" {}
}

module "gitlab_local_file" {
  source = "gitlab.com/mattkasa/gitlab-file/local"

  text = "Hello World"
  filename = "hello"
}

output "filesize_in_bytes" {
  value = module.gitlab_local_file.bytes
}
